#!/bin/bash

# Check if Node.js is already installed
if command -v node >/dev/null 2>&1; then
  echo "Node.js is already installed."
  echo "Skipping NVM installation and Node.js download."
else

  # URL de téléchargement de Node.js
  NODE_URL="https://nodejs.org/dist/v18.20.1/node-v18.20.1-linux-x64.tar.xz"

  # Téléchargement de Node.js
  echo "Téléchargement de Node.js..."
  curl -L $NODE_URL -o node.tar.xz

  # Vérification du téléchargement
  if [ ! -f node.tar.xz ]; then
    echo "Échec du téléchargement de Node.js."
    exit 1
  fi

  # Extraction de Node.js
  echo "Extraction de Node.js..."
  sudo tar -xvJf node.tar.xz

  # Installation de Node.js
  echo "Installation de Node.js..."
  sudo mv node-v18.20.1-linux-x64 /usr/local/bin/node

  # Vérification de l'installation
  echo "Vérification de l'installation de Node.js..."
  node -v

  # Suppression des fichiers temporaires
  echo "Suppression des fichiers temporaires..."
  sudo rm node.tar.xz

  echo "####################################   Node.js a été installé avec succès ! ###################################"

fi