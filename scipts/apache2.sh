if command -v apache2 >/dev/null 2>&1; then
  echo "Apache2 is already installed."
else
  echo "Apache2 is installing..."
  sudo apt install apache2 -y

  # Install PHP and required modules
  sudo apt install php libapache2-mod-php php-mysql -y

  # Restart Apache web server
  sudo systemctl restart apache2
  
  echo "Apache2 is installed !"
fi