#!/bin/bash

# # ###############################################    install phpmyadmin   ####################################################
# Définir la fonction pour vérifier si PHPMyAdmin est installé
is_phpmyadmin_installed() {
  # Vérifier si Apache est installé
  if ! is_apache_installed; then
    echo "Apache n'est pas installé."
    return 1
  fi

  # Vérifier si le fichier de configuration de PHPMyAdmin existe
  if ! [ -f /etc/phpmyadmin/config.inc.php ]; then
    echo "Le fichier de configuration de PHPMyAdmin n'existe pas."
    return 1
  fi

  # Vérifier si le service PHPMyAdmin est actif
  if ! systemctl is-active phpmyadmin.service >/dev/null 2>&1; then
    echo "Le service PHPMyAdmin n'est pas actif."
    return 1
  fi

  return 0
}

# Tester si PHPMyAdmin est installé
if is_phpmyadmin_installed; then
	echo "PHPMyAdmin est déjà installé."
else
  # Clean up any existing phpmyadmin packages before installation
  sudo apt remove --purge phpmyadmin php-mbstring php-gettext -y
  sudo apt autoremove -y
  
	echo "Installation de PHPMyAdmin en cours..."
	# Install PHPMyAdmin
	sudo apt install phpmyadmin -y

	# Prompt for PHPMyAdmin root password
	PMA_ROOT_PASS=""

	# Set PHPMyAdmin root password
	sudo mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY '$PMA_ROOT_PASS' WITH GRANT OPTION;"
	sudo mysql -e "FLUSH PRIVILEGES;"

	# Configure PHPMyAdmin with Apache
	sudo echo 'Include /etc/phpmyadmin/apache.conf' >> /etc/apache2/apache2.conf

	# Restart Apache web server
	sudo systemctl restart apache2
	echo "Installation de PHPMyAdmin terminée avec success !!"
fi