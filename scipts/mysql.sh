# # ###############################################    install mysql   ####################################################

# 1. Installation de MySQL Server

# Fonction pour vérifier si MySQL est installé
is_mysql_installed() {
  command -v mysql >/dev/null 2>&1
}

# Installation de MySQL si nécessaire
if ! is_mysql_installed; then
  echo "**MySQL n'est pas encore installé.**"
  echo "**Installation en cours...**"

  sudo apt install -y mysql-server

  echo "**Démarrage du service MySQL...**"
  sudo systemctl start mysql

  echo "**Sécurisation de l'installation MySQL...**"
  sudo mysql_secure_installation

  echo "**Installation de MySQL terminée !**"
else
  echo "**MySQL est déjà installé.**"
  echo "**Aucune installation nécessaire.**"
fi

# 2. Configuration de MySQL Server

# Informations de connexion à la base de données
DB_HOST="localhost"
DB_USERNAME="root"
DB_PASSWORD=""

# Nom et mot de passe du nouvel utilisateur
MYSQL_USER="userAnemiPrinter"
MYSQL_PASSWORD="userAnemiPrinter.dev@.2024"
MYSQL_ROOT_PASSWORD=""

# Nom de la nouvelle base de données
MYSQL_DATABASE="anemi_printer_dev_db"

# Connexion à la base de données
mysql -h $DB_HOST -u $DB_USERNAME -e "
CREATE DATABASE IF NOT EXISTS $MYSQL_DATABASE;   
CREATE USER IF NOT EXISTS '$MYSQL_USER'@'$DB_HOST' IDENTIFIED BY '$NEW_PASSWORD';   
GRANT ALL PRIVILEGES ON $MYSQL_DATABASE.* TO '$MYSQL_USER'@'$DB_HOST'; 
FLUSH PRIVILEGES;  
exit
"
echo "#############  Installation de mysql terminée ! ##############"
