#!/bin/bash

# Fonction pour vérifier si Java est déjà installé
java_est_installe() {
  if type -p java > /dev/null; then
    return 0
  else
    return 1
  fi
}

# Vérification de Java
if java_est_installe; then
  echo "Java est déjà installé."
  echo "Aucune installation nécessaire."
else
  echo "Java n'est pas installé."
  echo "Installation d'OpenJDK 17-jdk..."

  # Installation d'OpenJDK 17-jdk
  sudo apt update
  sudo apt install openjdk-17-jdk -y

  echo "Installation d'OpenJDK 17-jdk terminée."
fi
